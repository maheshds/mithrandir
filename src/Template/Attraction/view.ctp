<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'); ?>
    <?= $this->Html->css('bootstrap.min.css'); ?>
    <?= $this->Html->css('templatemo-style.css'); ?>
    <?= $this->Html->css('nv.d3.css'); ?>

    <title><?= $attraction->attractionName ?></title>
</head>

<body>
<div class="fixed-header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?= $attraction->attractionName ?> <span id="a_address"
                                                                                      style="font-size: small"></span></a>
        </div>
    </div>
</div>

<div class="container">
    <?= $this->Form->hidden('a_id', ['value' => $attraction->attraction_id, 'id' => 'a_id']); ?>
    <section class="col-md-12 content" id="home">
        <div class="col-lg-6 col-md-6 content-item" id="a_image">
            <?= $this->Html->image($attraction->image_url, ['alt' => 'Image', 'class' => 'tm-image']); ?>
        </div>
        <div class="col-lg-6 col-md-6 content-item content-item-1 background"
             style="background-color: rgba(110, 110, 110, 0.8);">
            <h2 class="main-title text-center dark-blue-text" style="color: yellow">Overview</h2>

            <p id="a_overview"></p>

        </div>
    </section>

    <section class="col-md-12 content padding" id="services" style="background-color: rgba(79, 79, 79, 0.8);">
        <h2 class="main-title text-center" style="color: yellow">Aspect Ratings</h2>

        <div style="height:100%; width:100%; overflow: hidden;">
            <div style="float: left; width:25%;" id="a_term_scenery">
                <h3 id="term" style=" text-align: center;color: #ebebec"></h3>
            </div>
            <div style="float: left; width:25%;" id="a_term_accommodation">
                <h3 id="term" style=" text-align: center;color: #ebebec"></h3>
            </div>
            <div style="float: left; width:25%;" id="a_term_ambiance">
                <h3 id="term" style=" text-align: center;color: #ebebec"></h3>
            </div>
            <div style="float: left; width:25%;" id="a_term_transportation">
                <h3 id="term" style=" text-align: center;color: #ebebec"></h3>
            </div>

        </div>


        <div style="height:100%; width:100%; overflow: hidden;">

            <div style="float: left; width:23%;" id="a_term_cost">
                <h3 id="term" style=" text-align: center;color: #ebebec"></h3>
            </div>
        </div>

    </section>


    <section class="col-md-12 content" id="clients">

        <div class="col-lg-6 col-md-6 content-item" style="background-color: rgba(110, 110, 110, 0.8);">
            <h2 class="main-title text-center" style="color: yellow">Best For...</h2>

            <div id="chart1" class='with-3d-shadow with-transitions'>
                <svg></svg>
            </div>

        </div>
        <div class="col-lg-6 col-md-6 content-item" style="background-color: rgba(110, 110, 110, 0.8);">

            <div class="shoutbox" style="overflow: auto;">

                <h2 class="main-title text-center" style="color: yellow">Community Reviews</h2>

                <ul class="shoutbox-content"></ul>


            </div>
        </div>
    </section>


</div>

<div class="text-center footer">
    <div class="container">
        Copyright @ 2016 Mahesh De Silva
    </div>
</div>

<?= $this->Html->script('jquery.min.js'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.singlePageNav.min.js'); ?>
<?= $this->Html->script('https://d3js.org/d3.v3.min.js'); ?>

<?= $this->Html->script('nv.d3.js'); ?>
<?= $this->Html->script('stream_layers.js'); ?>


<style>
    .progress-meter text {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 24px;
        font-weight: bold;
    }

    svg text {
        fill: white;
    }

    .shoutbox-content {
        list-style: none;
        width: 100%;
        margin: 0;
        padding: 0;
        color: #df377a;
        font-size: 14px;
        font-weight: 700;
        text-align: left;
        height: 400px;
        overflow-y: scroll;
    }

    .shoutbox-content li {
        padding: 20px;
        border-bottom: 1px solid #d5dbe1;
    }

    .shoutbox-username {
        display: inline;
        color: #DB7B26;
        font-size: 14px;
        font-weight: bold;
        text-align: left;
        margin-right: 5px;
    }

    .shoutbox-comment {
        display: inline;
        color: #ebebec;
        font-size: 14px;
        font-weight: 400;
        line-height: 21px;
        text-align: left;
    }

    .shoutbox-comment-details {
        width: 100%;
        margin: 5px auto;
        height: 11px;
    }

    .shoutbox-comment-reply {
        display: inline-block;
        float: left;
        color: #5896cb;
        font-size: 12px;
        font-weight: bold;
        line-height: 20px;
        text-align: left;
        height: 9px;
        cursor: pointer;
    }

    .shoutbox-comment-ago {
        display: inline-block;
        float: right;
        color: #9aa1a7;
        font-size: 11px;
        font-weight: normal;
        line-height: 20px;
        text-align: right;
        height: 11px;
    }

</style>
<script>

    // Check scroll position and add/remove background to navbar
    function checkScrollPosition() {
        if ($(window).scrollTop() > 50) {
            $(".fixed-header").addClass("scroll");
        } else {
            $(".fixed-header").removeClass("scroll");
        }
    }

    $(document).ready(function () {
        // Single page nav
        $('.fixed-header').singlePageNav({
            offset: 59,
            filter: ':not(.external)',
            updateHash: true
        });

        checkScrollPosition();

        // nav bar
        $('.navbar-toggle').click(function () {
            $('.main-menu').toggleClass('show');
        });

        $('.main-menu a').click(function () {
            $('.main-menu').removeClass('show');
        });
    });

    $(window).on("scroll", function () {
        checkScrollPosition();
    });


</script>
<script>
    var colors = {
        'pink': '#E1499A',
        'yellow': '#f0ff08',
        'green': '#47e495',
        'blue': "rgb(4, 57, 88)"
    };

    function appendNeonCircularThingy(start, end, divselect, color, term) {
        var radius = 100;
        var border = 5;
        var padding = 30;
        var startPercent = start;
        var endPercent = end;


        var twoPi = Math.PI * 2;
        var formatPercent = d3.format('.0%');
        var boxSize = (radius + padding) * 2;


        var count = Math.abs((endPercent - startPercent) / 0.01);
        var step = endPercent < startPercent ? -0.01 : 0.01;

        var arc = d3.svg.arc()
            .startAngle(0)
            .innerRadius(radius)
            .outerRadius(radius - border);

        var parent = d3.select(divselect);
        parent[0][0].children[0].innerText = term;
        var svg = parent.append('svg')
            .attr('width', boxSize)
            .attr('height', boxSize);

        var defs = svg.append('defs');

        var filter = defs.append('filter')
            .attr('id', 'blur');

        filter.append('feGaussianBlur')
            .attr('in', 'SourceGraphic')
            .attr('stdDeviation', '7');

        var g = svg.append('g')
            .attr('transform', 'translate(' + boxSize / 2 + ',' + boxSize / 2 + ')');

        var meter = g.append('g')
            .attr('class', 'progress-meter');

        meter.append('path')
            .attr('class', 'background')
            .attr('fill', '#ccc')
            .attr('fill-opacity', 0.5)
            .attr('d', arc.endAngle(twoPi));

        var foreground = meter.append('path')
            .attr('class', 'foreground')
            .attr('fill', color)
            .attr('fill-opacity', 1)
            .attr('stroke', color)
            .attr('stroke-width', 5)
            .attr('stroke-opacity', 1)
            .attr('filter', 'url(#blur)');

        var front = meter.append('path')
            .attr('class', 'foreground')
            .attr('fill', color)
            .attr('fill-opacity', 1);

        var numberText = meter.append('text')
            .attr('fill', '#fff')
            .attr('text-anchor', 'middle')
            .attr('dy', '.35em');

        function updateProgress(progress) {
            foreground.attr('d', arc.endAngle(twoPi * progress));
            front.attr('d', arc.endAngle(twoPi * progress));
            numberText.text(formatPercent(progress));
        }

        var progress = startPercent;

        (function loops() {
            updateProgress(progress);

            if (count > 0) {
                count--;
                progress += step;
                setTimeout(loops, 10);
            }
        })();
    }

    function getTopComment(id) {
        $.get("http://localhost/mithrandir/attraction/get_top_comments/" + id, function (res) {
            if (res !== null) {
                appendComments(JSON.parse(res));

            }
        });
    }

    $(document).ready(function () {
        var id = $('#a_id').val();
        $.get("http://localhost/mithrandir/attraction/view_attraction/" + id, function (res) {
            if (res !== null) {
                var result = JSON.parse(res);

                var jsonObject = result[0];
                for (var i = 0; i < jsonObject.aspectCategories[0].length; i++) {
                    var score = jsonObject.aspectCategories[0][i].score;
                    var category = jsonObject.aspectCategories[0][i].category;
                    appendNeonCircularThingy(0, score / 10, "div#a_term_" + category.toLowerCase(), colors.yellow
                        , category);
                }

                $('#a_overview').text(jsonObject.overview);
                $('#a_address').text(jsonObject.address);
                createBarChart(jsonObject.topAspectTerms[0]);
                getTopComment(id);
            }
        });
    });

    function createBarChart(topAspectTerms) {
        var values = [];

        for (var i = 0; i < topAspectTerms.length; i++) {
            var item = {};
            item['label'] = topAspectTerms[i].aspectTerm;
            item['value'] = topAspectTerms[i].score;
            values.push(item);
        }

        var data = [{
            key: 'Top Aspect Terms',
            values: values
        }];

        var chart;
        nv.addGraph(function () {
            var width = 460, height = 400;
            chart = nv.models.multiBarHorizontalChart()
                .x(function (d) {
                    return d.label
                })
                .y(function (d) {
                    return d.value
                })
                .yErr(function (d) {
                    return [-Math.abs(d.value * Math.random() * 0.3), Math.abs(d.value * Math.random() * 0.3)]
                })
                .barColor(d3.scale.category20().range())
                .duration(250)
                .margin({left: 100})
                .margin({right: 100})
                .showControls(false)
                .stacked(false);

            chart.yAxis.tickFormat(d3.format(',.2f'));

            chart.yAxis.axisLabel('Score');
            chart.xAxis.axisLabel('Terms').axisLabelDistance(20);

            d3.select('#chart1 svg').datum(data).transition().duration(500).call(chart).style({
                'width': width,
                'height': height
            });
            nv.utils.windowResize(chart.update);

            chart.dispatch.on('stateChange', function (e) {
                nv.log('New State:', JSON.stringify(e));
            });
            chart.state.dispatch.on('change', function (state) {
                nv.log('state', JSON.stringify(state));
            });
            return chart;
        });
    }

    function appendComments(data) {
        var ul = $('ul.shoutbox-content')
        ul.empty();
        data.forEach(function (d) {
            var reviwerName;
            if (d.reviewer == null) {
                reviwerName = "User(" + d.reviewer_id + ")";
            } else {
                reviwerName = d.reviewer.reviewerName;
            }

            ul.append('<li>' +
                '<span class="shoutbox-username">' + reviwerName + '</span>' +
                '<p class="shoutbox-comment">' + d.review + '</p>' +
                '<div class="shoutbox-comment-details"><span class="shoutbox-comment-reply" data-name="' + reviwerName + '">REPLY</span>' +
                '<span class="shoutbox-comment-ago">' + d.date + '</span></div>' +
                '</li>');
        });
    }

</script>
</body>
</html>