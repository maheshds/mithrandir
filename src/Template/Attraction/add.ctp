<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Attraction'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="attraction form large-9 medium-8 columns content">
    <?= $this->Form->create($attraction) ?>
    <fieldset>
        <legend><?= __('Add Attraction') ?></legend>
        <?php
            echo $this->Form->input('attractionName');
            echo $this->Form->input('address');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
