<?php
namespace App\Model\Table;

use App\Model\Entity\Review;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Review Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Reviews
 * @property \Cake\ORM\Association\BelongsTo $Attraction
 * @property \Cake\ORM\Association\BelongsTo $Reviewer
 */
class ReviewTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('review');
        $this->displayField('title');
        $this->primaryKey('review_id');

        $this->belongsTo('Reviews', [
            'foreignKey' => 'review_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Attraction', [
            'foreignKey' => 'attraction_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Reviewer', [
            'foreignKey' => 'reviewer_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Reviewer',[
            'foreignKey' => 'reviewer_id',
            'jointType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('review', 'create')
            ->notEmpty('review');

        $validator
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->numeric('reviewOverallScore')
            ->requirePresence('reviewOverallScore', 'create')
            ->notEmpty('reviewOverallScore');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['review_id'], 'Reviews'));
        $rules->add($rules->existsIn(['attraction_id'], 'Attraction'));
        $rules->add($rules->existsIn(['reviewer_id'], 'Reviewer'));
        return $rules;
    }
}
