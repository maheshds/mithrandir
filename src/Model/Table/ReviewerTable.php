<?php
namespace App\Model\Table;

use App\Model\Entity\Reviewer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reviewer Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Reviewers
 */
class ReviewerTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reviewer');
        $this->displayField('reviewer_id');
        $this->primaryKey('reviewer_id');

        $this->belongsTo('Reviewers', [
            'foreignKey' => 'reviewer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('reviewerName', 'create')
            ->notEmpty('reviewerName');

        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['reviewer_id'], 'Reviewers'));
        return $rules;
    }
}
