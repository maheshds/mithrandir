<?php
namespace App\Model\Table;

use App\Model\Entity\AspectTerm;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AspectTerms Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AspectTerms
 */
class AspectTermsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('aspect_terms');
        $this->displayField('aspect_term_id');
        $this->primaryKey('aspect_term_id');

        $this->belongsTo('AspectTerms', [
            'foreignKey' => 'aspect_term_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('aspectTerm', 'create')
            ->notEmpty('aspectTerm')
            ->add('aspectTerm', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['aspectTerm']));
        $rules->add($rules->existsIn(['aspect_term_id'], 'AspectTerms'));
        return $rules;
    }
}
