<?php
namespace App\Model\Table;

use App\Model\Entity\ReviewAspectTerm;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReviewAspectTerm Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AspectTerms
 * @property \Cake\ORM\Association\BelongsTo $Review
 */
class ReviewAspectTermTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('review_aspect_term');

        $this->belongsTo('AspectTerms', [
            'foreignKey' => 'aspect_term_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Review', [
            'foreignKey' => 'review_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->numeric('aspectTermSentimentScore')
            ->requirePresence('aspectTermSentimentScore', 'create')
            ->notEmpty('aspectTermSentimentScore');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['aspect_term_id'], 'AspectTerms'));
        $rules->add($rules->existsIn(['review_id'], 'Review'));
        return $rules;
    }
}
