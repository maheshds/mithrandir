<?php
namespace App\Model\Table;

use App\Model\Entity\Attraction;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Attraction Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Attractions
 */
class AttractionTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('attraction');
        $this->displayField('attraction_id');
        $this->primaryKey('attraction_id');

        $this->belongsTo('Attractions', [
            'foreignKey' => 'attraction_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Review');

        $this->hasMany('TopReviews', [
            'className' => 'Review',
            'foreignKey' => 'attraction_id',
            'strategy' => 'select',

            'conditions' => function ($e, $query) {
                $query->limit(10);
                return [];
            }
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('attractionName', 'create')
            ->notEmpty('attractionName')
            ->add('attractionName', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['attractionName']));
        $rules->add($rules->existsIn(['attraction_id'], 'Attractions'));
        return $rules;
    }
}
