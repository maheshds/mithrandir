<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReviewAspectCategory Entity.
 *
 * @property float $categorySentimentScore
 * @property int $category_id
 * @property \App\Model\Entity\Category $category
 * @property string $review_id
 * @property \App\Model\Entity\Review $review
 */
class ReviewAspectCategory extends Entity
{

}
