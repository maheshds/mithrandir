<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReviewAspectTerm Entity.
 *
 * @property float $aspectTermSentimentScore
 * @property int $aspect_term_id
 * @property \App\Model\Entity\AspectTerm $aspect_term
 * @property string $review_id
 * @property \App\Model\Entity\Review $review
 */
class ReviewAspectTerm extends Entity
{

}
