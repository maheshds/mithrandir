<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Review Entity.
 *
 * @property string $review_id
 * @property \App\Model\Entity\Review $review
 * @property string $title
 * @property string $date
 * @property float $reviewOverallScore
 * @property int $attraction_id
 * @property \App\Model\Entity\Attraction $attraction
 * @property int $reviewer_id
 * @property \App\Model\Entity\Reviewer $reviewer
 */
class Review extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'review_id' => false,
    ];
}
