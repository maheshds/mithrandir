<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Attraction Controller
 *
 * @property \App\Model\Table\AttractionTable $Attraction
 */
class AttractionController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Review');
        $this->loadModel('ReviewAspectCategory');
        $this->loadModel('ReviewAspectTerm');
        $this->loadModel('AspectTerms');
        $this->loadModel('Attraction');
        $this->loadComponent('RequestHandler');

    }

    public function viewAttraction($id)
    {
        $this->viewBuilder()->layout(false);
        $content = array();
        //get attraction data
        $attractionArr = $this->Attraction->findAllByAttractionId($id);
        //$attractionArr = json_encode($attractionArr);

        //get review data
        $conditions = array("Review.attraction_id" => $id);
        $reviewArr = $this->Review->find('all', array("conditions" => $conditions));


        //get review_aspect_category data
        $categoryScoresArr = $this->setCategoryScores($reviewArr);

        //get top 5 best aspect terms
        $aspectTerms = $this->getFrequentAspectTerms($reviewArr);
        $aspectTermScores = $this->getFrequentAspectTermScores($aspectTerms);
        $aspectTermNames = $this->getFrequentAspectTermNames($aspectTermScores);

        //Generate final JSON data
        $jsonData = $this->generateAttractionInfoJson($attractionArr, $categoryScoresArr, $aspectTermNames);
        echo json_encode($jsonData);
        die();

    }

    public function generateAttractionInfoJson($attractionArr, $categoryScoresArr, $aspectTermNames)
    {
        $attractionInfoArr = array();
        $finalAttractionArr = $attractionArr->toArray();
        array_push($attractionInfoArr, array("attraction_id" => $finalAttractionArr[0]["attraction_id"],
            "attractionName" => $finalAttractionArr[0]["attractionName"], "address" => $finalAttractionArr[0]["address"],
            "overview" => "", "aspectCategories" => array($categoryScoresArr),
            "topAspectTerms" => array($aspectTermNames)
        ));
        return $attractionInfoArr;

    }


    public function getFrequentAspectTerms($reviewArr)
    {

        $aspectTerms = array();
        $aspectTermFrequencyArr = array();

        foreach ($reviewArr as $reviewObj) {
            $aspectTermConditions = array("ReviewAspectTerm.review_id" => $reviewObj->review_id);
            $aspectTermsArr = $this->ReviewAspectTerm->find('all', array("conditions" => $aspectTermConditions));
            foreach ($aspectTermsArr as $termObj) {
                //array_push($aspectTerms, array("aspectSentimentScore" => $termObj->aspectTermSentimentScore, "aspect_term_id" => $termObj->aspect_term_id));

                array_push($aspectTermFrequencyArr, $termObj->aspect_term_id);
            }
        }

        $aspectTermCountArr = array_count_values($aspectTermFrequencyArr);

        arsort($aspectTermCountArr);
        $frequentAspectTermsArr = array();
        $sortedKeys = array_keys($aspectTermCountArr);
        for ($i = 0; $i < 5; $i++) {
            array_push($frequentAspectTermsArr, $sortedKeys[$i]);
        }


        return $frequentAspectTermsArr;
    }

    public function getFrequentAspectTermScores($frequentAspectTermsArr)
    {

        $frequentAspectTermScoresArr = array();

        foreach ($frequentAspectTermsArr as $termObj) {

            $termConditions = array("ReviewAspectTerm.aspect_term_id" => $termObj);
            $aspectTermScoresArr = $this->ReviewAspectTerm->find('all', array('fields' => array('aspectTermSentimentScore'), "conditions" => $termConditions, 'limit' => 1));
            $data = $aspectTermScoresArr->toArray();

            array_push($frequentAspectTermScoresArr, array("aspect_term_id" => $termObj, "score" => $data[0]["aspectTermSentimentScore"]));
        }
        return $frequentAspectTermScoresArr;
    }

    public function getFrequentAspectTermNames($aspectTermScores)
    {
        $frequentAspectTermNamesArr = array();

        foreach ($aspectTermScores as $termObj) {
            $termConditions = array("AspectTerms.aspect_term_id" => $termObj["aspect_term_id"]);
            $aspectTermNamesQuery = $this->AspectTerms->find('all', array('fields' => array('aspectTerm'), "conditions" => $termConditions, 'limit' => 1));
            $data = $aspectTermNamesQuery->toArray();

            array_push($frequentAspectTermNamesArr, array("aspect_term_id" => $termObj["aspect_term_id"],
                "aspectTerm" => $data[0]["aspectTerm"], "score" => $termObj["score"],));
        }
        return $frequentAspectTermNamesArr;
    }

    public function setCategoryScores($reviewArr)
    {

        $sceneryCategoryScore = $accommodationCategoryScore = $ambianceCategoryScore = $transportCategoryScore = $costCategoryScore = 0.0;

        foreach ($reviewArr as $reviewObj) {
            $categoryConditions = array("ReviewAspectCategory.review_id" => $reviewObj->review_id);
            $aspectCategoryArr = $this->ReviewAspectCategory->find('all', array("conditions" => $categoryConditions));
            $index = 0;
            foreach ($aspectCategoryArr as $obj) {
                $index++;
                $categoryObj = $obj;
                switch ($index) {

                    case 1:
                        $sceneryCategoryScore += $categoryObj['categorySentimentScore'];


                    case 2:
                        $accommodationCategoryScore += $categoryObj['categorySentimentScore'];


                    case 3:
                        $ambianceCategoryScore += $categoryObj['categorySentimentScore'];


                    case 4:
                        $transportCategoryScore += $categoryObj['categorySentimentScore'];


                    case 5:
                        $costCategoryScore += $categoryObj['categorySentimentScore'];


                    default:
                        //do nothing
                }

            }

        }
        $reviewCount = count($reviewArr->toArray());
        $sceneryCategoryScore = $sceneryCategoryScore / $reviewCount;
        $accommodationCategoryScore = $accommodationCategoryScore / $reviewCount;
        $ambianceCategoryScore = $ambianceCategoryScore / $reviewCount;
        $transportCategoryScore = $transportCategoryScore / $reviewCount;
        $costCategoryScore = $costCategoryScore / $reviewCount;

        return array(array("category" => "Scenery", "score" => $sceneryCategoryScore),
            array("category" => "Accommodation", "score" => $accommodationCategoryScore),
            array("category" => "Ambiance", "score" => $ambianceCategoryScore),
            array("category" => "Transportation", "score" => $transportCategoryScore),
            array("category" => "Cost", "score" => $costCategoryScore),
        );
        //end of category score assignment.
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $attraction = $this->paginate($this->Attraction);

        $this->set(compact('attraction'));
        $this->set('_serialize', ['attraction']);
    }

    /**
     * View method
     *
     * @param string|null $id Attraction id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->layout(false);
        $attraction = $this->Attraction->get($id, [
            'contain' => ['TopReviews']
        ]);


        $this->set('attraction', $attraction);
        $this->set('_serialize', ['attraction']);
    }

    public function getTopComments($id = null)
    {
        $this->viewBuilder()->layout(false);
        $attraction = $this->Attraction->get($id, [
            'contain' => ['TopReviews','TopReviews.Reviewer']
        ]);

        echo json_encode($attraction->top_reviews);

       die();
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $attraction = $this->Attraction->newEntity();
        if ($this->request->is('post')) {
            $attraction = $this->Attraction->patchEntity($attraction, $this->request->data);
            if ($this->Attraction->save($attraction)) {
                $this->Flash->success(__('The attraction has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The attraction could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('attraction'));
        $this->set('_serialize', ['attraction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Attraction id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $attraction = $this->Attraction->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attraction = $this->Attraction->patchEntity($attraction, $this->request->data);
            if ($this->Attraction->save($attraction)) {
                $this->Flash->success(__('The attraction has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The attraction could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('attraction'));
        $this->set('_serialize', ['attraction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Attraction id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attraction = $this->Attraction->get($id);
        if ($this->Attraction->delete($attraction)) {
            $this->Flash->success(__('The attraction has been deleted.'));
        } else {
            $this->Flash->error(__('The attraction could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
