<?php
namespace App\Controller;

use App\Controller\AppController;
use Predis\Client;

/**
 * Search Controller
 *
 * @property \App\Model\Table\SearchTable $Search
 */
class SearchController extends AppController
{

    private $redisClient;


    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Review');
        $this->loadComponent('RequestHandler');

        $this->redisClient = new Client(array(
            'host' => "localhost",
            'port' => 6379,
        ));
    }

    /**
     * View method
     *
     * @param string|null $id Search id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */


    public function content()
    {
        $this->viewBuilder()->layout(false);
        if ($this->request->is('post')) {
            $postObject = $this->request->input('json_decode');
            $response = array();
            if (!empty($postObject)) {
                $json = get_object_vars($postObject);
                $searchQuery = trim($json['search_query']);

                $searchQuery = explode(" ", trim($searchQuery));
                $sets = array();
                for ($i = 0, $length = count($searchQuery); $i < $length; $i++) {
                    $query = strtolower($searchQuery[$i]);
                    $keys = $this->redisClient->keys('keyword:' . $query . '*');
                    $sets = array_merge($sets, $keys);
                }

                $result = $this->redisClient->sunion($sets);

                foreach ($result as $attractionId) {
                    $attractionObject = $this->redisClient->get('attraction:' . $attractionId);
                    array_push($response, json_decode($attractionObject));
                }

                echo json_encode($response);


            }
        }
        die();
    }


    public function locations()
    {
        $this->viewBuilder()->layout(false);
        if ($this->request->is('get')) {
            $response = array();

            $keys = $this->redisClient->keys('attraction*');

            foreach ($keys as $attraction) {
                $attractionObject = $this->redisClient->get($attraction);
                array_push($response, json_decode($attractionObject));

            }

            echo json_encode($response);
        }

        die();
    }


    public function view($id = null)
    {
        $search = $this->Search->get($id, [
            'contain' => []
        ]);

        $this->set('search', $search);
        $this->set('_serialize', ['search']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $search = $this->Search->newEntity();
        if ($this->request->is('post')) {
            $search = $this->Search->patchEntity($search, $this->request->data);
            if ($this->Search->save($search)) {
                $this->Flash->success(__('The search has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The search could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('search'));
        $this->set('_serialize', ['search']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Search id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $search = $this->Search->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $search = $this->Search->patchEntity($search, $this->request->data);
            if ($this->Search->save($search)) {
                $this->Flash->success(__('The search has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The search could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('search'));
        $this->set('_serialize', ['search']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Search id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $search = $this->Search->get($id);
        if ($this->Search->delete($search)) {
            $this->Flash->success(__('The search has been deleted.'));
        } else {
            $this->Flash->error(__('The search could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
