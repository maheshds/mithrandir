<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReviewFixture
 *
 */
class ReviewFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'review';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'review_id' => ['type' => 'string', 'length' => 35, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'review' => ['type' => 'string', 'length' => 2000, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'date' => ['type' => 'string', 'length' => 35, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'reviewOverallScore' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'attraction_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'reviewer_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'attractionId' => ['type' => 'index', 'columns' => ['attraction_id'], 'length' => []],
            'reviewerId' => ['type' => 'index', 'columns' => ['reviewer_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['review_id'], 'length' => []],
            'review_ibfk_1' => ['type' => 'foreign', 'columns' => ['attraction_id'], 'references' => ['attraction', 'attraction_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'review_ibfk_2' => ['type' => 'foreign', 'columns' => ['reviewer_id'], 'references' => ['reviewer', 'reviewer_id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'review_id' => 'cbd9b6c5-5e25-4f18-b63d-305bcd89bf7c',
            'title' => 'Lorem ipsum dolor sit amet',
            'review' => 'Lorem ipsum dolor sit amet',
            'date' => 'Lorem ipsum dolor sit amet',
            'reviewOverallScore' => 1,
            'attraction_id' => 1,
            'reviewer_id' => 1
        ],
    ];
}
