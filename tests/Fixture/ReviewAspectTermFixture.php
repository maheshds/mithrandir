<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReviewAspectTermFixture
 *
 */
class ReviewAspectTermFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'review_aspect_term';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'aspectTermSentimentScore' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'aspect_term_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'review_id' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'aspectTermId' => ['type' => 'index', 'columns' => ['aspect_term_id'], 'length' => []],
            'reviewId' => ['type' => 'index', 'columns' => ['review_id'], 'length' => []],
        ],
        '_constraints' => [
            'review_aspect_term_ibfk_1' => ['type' => 'foreign', 'columns' => ['aspect_term_id'], 'references' => ['aspect_terms', 'aspect_term_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'review_aspect_term_ibfk_2' => ['type' => 'foreign', 'columns' => ['review_id'], 'references' => ['review', 'review_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'aspectTermSentimentScore' => 1,
            'aspect_term_id' => 1,
            'review_id' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
