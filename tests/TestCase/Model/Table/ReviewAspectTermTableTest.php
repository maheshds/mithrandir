<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewAspectTermTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewAspectTermTable Test Case
 */
class ReviewAspectTermTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewAspectTermTable
     */
    public $ReviewAspectTerm;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.review_aspect_term',
        'app.aspect_terms',
        'app.review',
        'app.reviews',
        'app.attraction',
        'app.attractions',
        'app.reviewer'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReviewAspectTerm') ? [] : ['className' => 'App\Model\Table\ReviewAspectTermTable'];
        $this->ReviewAspectTerm = TableRegistry::get('ReviewAspectTerm', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReviewAspectTerm);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
