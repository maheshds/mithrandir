<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewAspectCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewAspectCategoryTable Test Case
 */
class ReviewAspectCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewAspectCategoryTable
     */
    public $ReviewAspectCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.review_aspect_category',
        'app.categories',
        'app.review',
        'app.reviews',
        'app.attraction',
        'app.attractions',
        'app.reviewer'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReviewAspectCategory') ? [] : ['className' => 'App\Model\Table\ReviewAspectCategoryTable'];
        $this->ReviewAspectCategory = TableRegistry::get('ReviewAspectCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReviewAspectCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
