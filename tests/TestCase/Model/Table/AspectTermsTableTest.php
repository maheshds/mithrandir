<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AspectTermsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AspectTermsTable Test Case
 */
class AspectTermsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AspectTermsTable
     */
    public $AspectTerms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.aspect_terms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AspectTerms') ? [] : ['className' => 'App\Model\Table\AspectTermsTable'];
        $this->AspectTerms = TableRegistry::get('AspectTerms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AspectTerms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
