<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewerTable Test Case
 */
class ReviewerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewerTable
     */
    public $Reviewer;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reviewer',
        'app.reviewers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Reviewer') ? [] : ['className' => 'App\Model\Table\ReviewerTable'];
        $this->Reviewer = TableRegistry::get('Reviewer', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Reviewer);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
